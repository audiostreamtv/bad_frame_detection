/*
 * calculate_his.cpp
 *
 *  Created on: Jun 10, 2014
 *      Author: chenge
 */

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/video/background_segm.hpp"

#include <iostream>
#include <stdio.h>
#include <math.h>

using namespace std;
using namespace cv;

/**
 * @function main
 */
int main( int argc, char** argv )
{
  Mat src_base, src_input;
  Mat src_base2,src_base3,src_base4;
  const char* im_path= "/home/chenge/workspace/bad_frame_detection/frame_data/Captures/good26.jpeg";
  const char* filename = argc >=2 ? argv[1] : im_path;

  /// Load image
     src_input = imread( filename, 1 );
     if( src_input.empty()){
     	cout<<"No such frame!! Please check the path."<<endl;
         return -1;}

     vector<Mat> bgr_planes_input;
     split( src_input, bgr_planes_input );

  /// Establish the number of bins
  int histSize = 256;

  /// Set the ranges ( for B,G,R) )
  float range[] = { 0, 256 } ;
  const float* histRange = { range };
  //bins to have the same size (uniform) and to clear the histograms in the beginning
  bool uniform = true; bool accumulate = false;

  Mat input_b_hist,input_g_hist,input_r_hist;

  calcHist( &bgr_planes_input[0], 1, 0, Mat(), input_b_hist, 1, &histSize, &histRange, uniform, accumulate );
  calcHist( &bgr_planes_input[1], 1, 0, Mat(), input_g_hist, 1, &histSize, &histRange, uniform, accumulate );
  calcHist( &bgr_planes_input[2], 1, 0, Mat(), input_r_hist, 1, &histSize, &histRange, uniform, accumulate );




/*
        /// Apply the histogram comparison methods (in total 4 methods)
        for( int i = 0; i < 4; i++ )
        {
            int compare_method = i;
            double base_base = compareHist( hist_base, hist_base, compare_method );
            double base_half = compareHist( hist_base, hist_half_down, compare_method );
            double base_input = compareHist( hist_base, hist_input, compare_method );

            printf( " Method [%d] base-Base, base_half, base-Input : %f, %f, %f,\n", i, base_base, base_half,base_input );

            // the higher the score is, we are more confident this is a BAD image
            if (base_base == 0){
            	if (base_input>base_half)
            		score+=1;
            }

            if (base_base!=0){
            	if (base_input<base_half)
            		score+=1;
            }
cout<<"score is: "<<score<<endl;
        }
*/

/////////////////////////////////////////////////////
///////////////////////////display the histogram



///I take the image histogram for each channel, and calculate the ratio of bins that are higher
  ///than a certain percentage of the peak (50% of the peak).
  ///If this ratio is high for any of the color channels, the image is good, otherwise it is bad.

double minVal_b,minVal_g,minVal_r; 
double maxVal_b,maxVal_g,maxVal_r; 
Point minLoc; 
Point maxLoc;

minMaxLoc( input_b_hist, &minVal_b, &maxVal_b, &minLoc, &maxLoc );
minMaxLoc( input_g_hist, &minVal_g, &maxVal_g, &minLoc, &maxLoc );
minMaxLoc( input_r_hist, &minVal_r, &maxVal_r, &minLoc, &maxLoc );
cout << "min val : " << minVal_b << endl<< minVal_g<<endl<< minVal_r<<endl;
cout << "max val: " << maxVal_b << endl<<maxVal_g << endl<< maxVal_r<<endl;

//normalize the histograms
normalize( input_b_hist, input_b_hist, 0, float (0.5*maxVal_b), NORM_MINMAX, -1, Mat() );
normalize( input_g_hist, input_g_hist, 0, float (0.5*maxVal_g), NORM_MINMAX, -1, Mat() );
normalize( input_r_hist, input_r_hist, 0, float (0.5*maxVal_r), NORM_MINMAX, -1, Mat() );


int bin_count[3]={0, 0, 0};
for(int i=1;i<input_b_hist.rows;i++){
	float b_bin=input_b_hist.at<float>(i);
	float g_bin=input_g_hist.at<float>(i);
	float r_bin=input_r_hist.at<float>(i);

	if (b_bin>=float (0.06*maxVal_b)){ bin_count[0]+=1;	}
	if (g_bin>=float (0.06*maxVal_g)){ bin_count[1]+=1;	}
	if (r_bin>=float (0.06*maxVal_r)){ bin_count[2]+=1;	}

}

std::cout<<"bin_count[0]:"<<bin_count[0]<< std::endl;
std::cout<<"bin_count[1]"<<bin_count[1]<< std::endl;
std::cout<<"bin_count[2]"<<bin_count[2]<< std::endl;
std::cout<<"0.3*maxVal:  "<<float (0.3*maxVal_g)<<endl<<float (0.3*maxVal_b)<<endl<<float (0.3*maxVal_r)<< std::endl;
cout<<int(0.10*input_b_hist.rows) <<endl;

	//// Display the histogram
	  int hist_w = 512; int hist_h = 400;
	  int bin_w = cvRound( (double) hist_w/histSize );
	  Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );
	 for( int i = 1; i < histSize; i++ )
	    {
	        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(input_b_hist.at<float>(i-1)) ) ,
	                         Point( bin_w*(i), hist_h - cvRound(input_b_hist.at<float>(i)) ),
	                         Scalar( 0, 0, 255), 2, 8, 0  );
	     //   debug
	     //    std::cout<<"base_hist:"<<(int (cvRound(base_b_hist.at<float>(i-1))))<< std::endl;


	        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(input_g_hist.at<float>(i-1)) ) ,
	                         Point( bin_w*(i), hist_h - cvRound(input_g_hist.at<float>(i)) ),
	                         Scalar( 0, 255, 0), 2, 8, 0  );

	        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(input_r_hist.at<float>(i-1)) ) ,
	                         Point( bin_w*(i), hist_h - cvRound(input_r_hist.at<float>(i)) ),
	                         Scalar( 255, 0, 0), 2, 8, 0  );
	          // debug
	       // std::cout<<"input_g_hist:"<<(int (cvRound(input_g_hist.at<float>(i-1))))<< std::endl;


	    }


	        // Display
	          namedWindow("calcHist Demo", CV_WINDOW_AUTOSIZE );
	          imshow("calcHist Demo", histImage );

	          waitKey();


int color_even= 0;
for (int k=0;k<=2;k++){
if (bin_count[k]>=int(0.1*input_b_hist.rows))
	color_even+=1;
}
cout<<"color_even: "<<color_even<<endl;
//if (color_even>=2 )
//	{cout<<"This is a good frame."<<endl;}
//else{

 ///////////////////////////////////////////////////////////////////////
 ////////////////////Discrete Fourier Transform////////////////////////

    Mat I = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);


    Mat padded;                            //expand input image to optimal size
    int m = getOptimalDFTSize( I.rows );
    int n = getOptimalDFTSize( I.cols ); // on the border add zero values
    copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_CONSTANT, Scalar::all(0));

    Mat planes[] = {Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F)};
    Mat complexI;
    merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

    dft(complexI, complexI);            // this way the result may fit in the source matrix

    // compute the magnitude and switch to logarithmic scale
    // => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
    split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
    magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
    Mat magI = planes[0];

    magI += Scalar::all(1);                    // switch to logarithmic scale
    log(magI, magI);

    // crop the spectrum, if it has an odd number of rows or columns
    magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));

    // rearrange the quadrants of Fourier image  so that the origin is at the image center
    int cx = magI.cols/2;
    int cy = magI.rows/2;

    Mat q0(magI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
    Mat q1(magI, Rect(cx, 0, cx, cy));  // Top-Right
    Mat q2(magI, Rect(0, cy, cx, cy));  // Bottom-Left
    Mat q3(magI, Rect(cx, cy, cx, cy)); // Bottom-Right

    //before swap, the high frequency components are in the center.
    Mat high_component(magI, Rect(int(cx*3/4), int(cy*3/4), int(cx/2), int(cy/2)));  //the center: highest frequecy

    float energy=0.0,high_comp_energy=0.0;
    for(int i=1;i<magI.rows;i++){
    	for(int j=1;j<magI.cols;j++){
    		float element=magI.at<float>(i,j);
    		energy+=pow(element,2.0);
    	}
    }

    for(int i=1;i<high_component.rows;i++){
        	for(int j=1;j<high_component.cols;j++){
        		float element=high_component.at<float>(i,j);
        		high_comp_energy+=pow(element,2.0);
        	}
    }
    cout<<"whole energy is: "<<energy<<endl<<"high frequency component energy is: "<<high_comp_energy<<endl;
    cout<<"high_comp_energy/energy: "<<float(high_comp_energy/energy)<<endl;


    //Decision!
   if ((color_even >=2)&& (high_comp_energy<=energy/20)&&(energy!=0))
   {
    	std::cout<< "Input image is a good frame."<<std::endl;}

  //Only those images whose color distribution is very even (color_even>=2), and don't have high-frequency components
  // energy will be treated as good images.
   else
	   std::cout<< "Input image is a bad frame."<<std::endl;


  //swap to display the spectrum:
        Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
        q0.copyTo(tmp);
        q3.copyTo(q0);
        tmp.copyTo(q3);

        q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
        q2.copyTo(q1);
        tmp.copyTo(q2);

        normalize(magI, magI, 0, 1, CV_MINMAX); // Transform the matrix with float values into a
                                                // viewable image form (float between values 0 and 1).

        imshow("Input Image"       , I   );    // Show the result
        imshow("spectrum magnitude", magI);

        normalize(high_component, high_component, 0, 1, CV_MINMAX);
        imshow("high f component",high_component);
        waitKey();

    //}
    return 0;
}




















