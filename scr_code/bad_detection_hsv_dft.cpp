/*
 * calculate_his.cpp
 *
 *  Created on: Jun 10, 2014
 *      Author: chenge
 */

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/video/background_segm.hpp"

#include <iostream>
#include <stdio.h>
#include <math.h>

using namespace std;
using namespace cv;

/**
 * @function main
 */
int main( int argc, char** argv )
{
  Mat src_base, src_input;
  Mat src_base2,src_base3,src_base4;
  const char* im_path= "/home/chenge/Downloads/1tv.jpg";
  const char* filename = argc >=2 ? argv[1] : im_path;

  /// Load image
     src_input = imread( filename, 1 );

     /////////////////////////////////////////////////////////////
     ////////////////color histogram comparison///////////////////

     src_base = imread( "/home/chenge/Documents/Bad_Frame_data/goodImage1.jpeg", 1 );
     src_base2 = imread( "/home/chenge/Documents/Bad_Frame_data/goodImage2.jpeg", 1 );
     src_base3 = imread( "/home/chenge/Documents/Bad_Frame_data/goodImage3.jpeg", 1 );
     src_base4 = imread( "/home/chenge/Documents/Bad_Frame_data/goodImage4.jpeg", 1 );

/// Convert to HSV
    Mat hsv_base, hsv_base2,hsv_base3,hsv_base4,hsv_input,hsv_half_down;
    cvtColor( src_base, hsv_base, COLOR_BGR2HSV );
    cvtColor( src_base2, hsv_base2, COLOR_BGR2HSV );
    cvtColor( src_base3, hsv_base3, COLOR_BGR2HSV );
    cvtColor( src_base4, hsv_base4, COLOR_BGR2HSV );
    cvtColor( src_input, hsv_input, COLOR_BGR2HSV );

    hsv_half_down = hsv_base( Range( hsv_base.rows/2, hsv_base.rows - 1 ), Range( 0, hsv_base.cols - 1 ) );

    /// Using 50 bins for hue and 60 for saturation
    int h_bins = 50; int s_bins = 60;
    int histSize[] = { h_bins, s_bins };

    // hue varies from 0 to 179, saturation from 0 to 255
    float h_ranges[] = { 0, 180 };
    float s_ranges[] = { 0, 256 };

    const float* ranges[] = { h_ranges, s_ranges };

    // Use the o-th and 1-st channels
    int channels[] = { 0, 1 };
    int score=0;


    /// Histograms
    MatND hist_base;
    MatND hist_half_down;
    MatND hist_input,hist_base2,hist_base3,hist_base4;

    /// Calculate the histograms for the HSV images
    calcHist( &hsv_base, 1, channels, Mat(), hist_base, 2, histSize, ranges, true, false );
    normalize( hist_base, hist_base, 0, 1, NORM_MINMAX, -1, Mat() );

    calcHist( &hsv_base2, 1, channels, Mat(), hist_base2, 2, histSize, ranges, true, false );
    normalize( hist_base2, hist_base2, 0, 1, NORM_MINMAX, -1, Mat() );

    calcHist( &hsv_base3, 1, channels, Mat(), hist_base3, 2, histSize, ranges, true, false );
    normalize( hist_base3, hist_base3, 0, 1, NORM_MINMAX, -1, Mat() );

    calcHist( &hsv_base4, 1, channels, Mat(), hist_base4, 2, histSize, ranges, true, false );
    normalize( hist_base4, hist_base4, 0, 1, NORM_MINMAX, -1, Mat() );

    calcHist( &hsv_half_down, 1, channels, Mat(), hist_half_down, 2, histSize, ranges, true, false );
    normalize( hist_half_down, hist_half_down, 0, 1, NORM_MINMAX, -1, Mat() );

    calcHist( &hsv_input, 1, channels, Mat(), hist_input, 2, histSize, ranges, true, false );
    normalize( hist_input, hist_input, 0, 1, NORM_MINMAX, -1, Mat() );

      //get the average histogram

    hist_base=hist_base+hist_base2+hist_base3+hist_base4;
    normalize( hist_base, hist_base, 0, 1, NORM_MINMAX, -1, Mat() );



        /// Apply the histogram comparison methods (in total 4 methods)
        for( int i = 0; i < 4; i++ )
        {
            int compare_method = i;
            double base_base = compareHist( hist_base, hist_base, compare_method );
            double base_half = compareHist( hist_base, hist_half_down, compare_method );
            double base_input = compareHist( hist_base, hist_input, compare_method );

            printf( " Method [%d] base-Base, base_half, base-Input : %f, %f, %f,\n", i, base_base, base_half,base_input );

            // the higher the score is, we are more confident this is a BAD image
            if (base_base == 0){
            	if (base_input>base_half)
            		score+=1;
            }

            if (base_base!=0){
            	if (base_input<base_half)
            		score+=1;
            }
cout<<"score is: "<<score<<endl;
        }

 ///////////////////////////////////////////////////////////////////////
 ////////////////////Discrete Fourier Transform////////////////////////

    Mat I = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
    if( I.empty())
        return -1;

    Mat padded;                            //expand input image to optimal size
    int m = getOptimalDFTSize( I.rows );
    int n = getOptimalDFTSize( I.cols ); // on the border add zero values
    copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_CONSTANT, Scalar::all(0));

    Mat planes[] = {Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F)};
    Mat complexI;
    merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

    dft(complexI, complexI);            // this way the result may fit in the source matrix

    // compute the magnitude and switch to logarithmic scale
    // => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
    split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
    magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
    Mat magI = planes[0];

    magI += Scalar::all(1);                    // switch to logarithmic scale
    log(magI, magI);

    // crop the spectrum, if it has an odd number of rows or columns
    magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));

    // rearrange the quadrants of Fourier image  so that the origin is at the image center
    int cx = magI.cols/2;
    int cy = magI.rows/2;

    Mat q0(magI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
    Mat q1(magI, Rect(cx, 0, cx, cy));  // Top-Right
    Mat q2(magI, Rect(0, cy, cx, cy));  // Bottom-Left
    Mat q3(magI, Rect(cx, cy, cx, cy)); // Bottom-Right


    Mat high_component(magI, Rect(int(cx*3/4), int(cy*3/4), int(cx/2), int(cy/2)));  //the center: highest frequecy

    //don't swap.
    //The high frequency components are in the center.

//    Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
//    q0.copyTo(tmp);
//    q3.copyTo(q0);
//    tmp.copyTo(q3);
//
//    q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
//    q2.copyTo(q1);
//    tmp.copyTo(q2);

//    normalize(magI, magI, 0, 1, CV_MINMAX); // Transform the matrix with float values into a
//                                            // viewable image form (float between values 0 and 1).
//
//    imshow("Input Image"       , I   );    // Show the result
//    imshow("spectrum magnitude", magI);
//
//    normalize(high_component, high_component, 0, 1, CV_MINMAX);
//    imshow("high f com",high_component);
//    waitKey();


    float energy=0.0,high_comp_energy=0.0;
    for(int i=1;i<magI.rows;i++){
    	for(int j=1;j<magI.cols;j++){
    		float element=magI.at<float>(i,j);
    		energy+=pow(element,2.0);
    	}
    }

    for(int i=1;i<high_component.rows;i++){
        	for(int j=1;j<high_component.cols;j++){
        		float element=high_component.at<float>(i,j);
        		high_comp_energy+=pow(element,2.0);
        	}
    }
    cout<<"whole energy is: "<<energy<<endl<<"high frequency component energy is: "<<high_comp_energy<<endl;

    //Decision!
            if ((score>2 && high_comp_energy>energy/25)||energy==high_comp_energy){
            	 std::cout<< "Input image is a bad frame."<<" score="<<score<<std::endl;}
            else std::cout<< "Input image is a good frame."<<" score="<<score<<std::endl;

            return 0;
}




















