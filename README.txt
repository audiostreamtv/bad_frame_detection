/////Chenge Li, cl2840@nyu.edu


=====================INTRO:
This is a bad frame detection repos.
Bad frames are distinguished based on their color distribution in RGB and their spectrum.


=====================A
Calculate historgrams for R, G, B channels.
Get the maximum for each historgram.

Normalize the historgrams by half of their maximum.
Count the number of bins that are larger than a threshold. (0.06*maxVal) 
/////(It's equvalent to threshold the original bins (before normalization) by 0.12*maxVal_b.)

If the color is ditributed evenly, bin_count for each channel will be bigger than a threshold. (0.1*hist.rows)


=====================B
Do the 2D DFT for the input image.
Before swapping, the highest frequency components are in the center of the spectrum.
Get the small area in the center and calculate its energy.
Compare the high frequency energy with the whole image energy, and get the ratio. Bad images tend to have higher ratio.


=====================Decision
The image will be classified as good image only if the histogram is distributed evenly on at least two color channels and the high frequency energy is not very high and the energy is not zero.

Otherwise the image will be treated as a bad/corrupted one.
